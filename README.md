# Open Interview Questions

Send your responses to jschatz@gitlab.com. Use subject line: `[GitLab OIQ]` followed by your name. 

We will reply to every email. Please include your first and last name, resume, and why you want to apply at GitLab. No cover letters please.

## Slow MRs

Analyze a slow MR. Find 1 merge request with at least 100 comments. e.g 

* gitlab-org/gitlab-ce!12069
* gitlab-org/gitlab-ce!10319
* gitlab-org/gitlab-ce!8844 

These merge requests are slow performance wise. They are slow to load, slow to make comments, and slow to interact with in general.
Do a complete analysis of one of these merge requests and tell us why they are slow.
Show us how you arrived at your conclusion. Make suggestions on how to improve the speed.